const std = @import("std");
const testing = std.testing;
const ArrayList = std.ArrayList;
const test_allocator = std.testing.allocator;
const Allocator = std.mem.Allocator;

pub const BaseResult = struct {
    res: ArrayList(u8),
    buff: ArrayList(u8),
    encoder: std.base64.Base64Encoder,

    pub fn init(allocator: Allocator, str_len: u64) !BaseResult {
        var buff = ArrayList(u8).init(allocator);
        try buff.ensureTotalCapacityPrecise(str_len / 2);

        var encoder = std.base64.standard.Encoder;

        var res = ArrayList(u8).init(allocator);
        try res.ensureTotalCapacityPrecise(encoder.calcSize(buff.allocatedSlice().len));

        return BaseResult{ .res = res, .buff = buff, .encoder = encoder };
    }

    pub fn deinit(self: *BaseResult) void {
        self.res.deinit();
        self.buff.deinit();
    }

    pub fn to_result(self: *BaseResult) ![]const u8 {
        return self.encoder.encode(self.res.allocatedSlice(), self.buff.allocatedSlice());
    }
};

pub fn from_hex(str: []const u8, buff: *ArrayList(u8)) !*ArrayList(u8) {
    try buff.ensureTotalCapacityPrecise(str.len / 2);

    var idx: u64 = 0;
    while (idx < str.len) : (idx += 2) {
        var dec = try std.fmt.parseInt(u8, str[idx .. idx + 2], 16);
        try buff.append(dec);
    }

    return buff;
}

pub fn hex_to_base64(str: []const u8, allocator: Allocator) !ArrayList(u8) {
    var buff = ArrayList(u8).init(allocator);
    defer buff.deinit();
    _ = try from_hex(str, &buff);

    const encoder = std.base64.standard.Encoder;

    var res = ArrayList(u8).init(allocator);
    // const cap = baseEncoder.calcSize(buff.allocatedSlice().len);
    const cap = encoder.calcSize(buff.allocatedSlice().len);
    try res.ensureTotalCapacityPrecise(cap);
    _ = encoder.encode(res.allocatedSlice(), buff.allocatedSlice());

    return res;
}
const CustomError = error{InvalidInput};

pub fn fixed_len_xor(str: []const u8, other: []const u8, allocator: Allocator) !ArrayList(u8) {
    if (str.len != other.len) {
        return CustomError.InvalidInput;
    }

    var str_buff = ArrayList(u8).init(allocator);
    defer str_buff.deinit();
    _ = try from_hex(str, &str_buff);

    var other_buff = ArrayList(u8).init(allocator);
    defer other_buff.deinit();
    _ = try from_hex(other, &other_buff);

    var res = ArrayList(u8).init(allocator);

    for (str_buff.allocatedSlice(), 0..) |value, idx| {
        try res.append(value ^ other_buff.allocatedSlice()[idx]);
    }

    return res;
}

// test "sanity" {
// try testing.expectEqual(1 + 1, 2);
// }

test "fixed_len_xor" {
    var actual = try fixed_len_xor("1c0111001f010100061a024b53535009181c", "686974207468652062756c6c277320657965", test_allocator);
    defer actual.deinit();
    const expected = "746865206b696420646f6e277420706c6179";

    const res = actual.allocatedSlice();

    const rss: *const [expected.len / 2]u8 = @ptrCast(res);

    try testing.expectEqualStrings(expected, &std.fmt.bytesToHex(rss.*, std.fmt.Case.lower));
}

test "hex_to_base64" {
    var result = try hex_to_base64("49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d", test_allocator);
    defer result.deinit();

    const actual = result.allocatedSlice();

    const expected = "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t";
    try testing.expectEqualStrings(expected, actual);
}
